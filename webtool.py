#!/usr/bin/env python3
import os
from flask import Flask, flash, request, redirect, render_template, flash, session, abort, url_for
from werkzeug.utils import secure_filename
import os.path
from flask_autoindex import AutoIndex
import flask_login
from flask_login import LoginManager
from dotenv import load_dotenv


load_dotenv() #Для загрузки дополнительной переменной окружения из файла .env не имзеняя основную
user = os.environ['userweb']          # Переменная для пользователя, admin по умолчанию
password = os.environ['passwordweb']      # Переменная для пароль, admin по умолчанию
app = Flask(__name__)
app.config['SECRET_KEY'] = 'gjhgjhgjg65jgjhfjfjkjhjkhjbkb'      # Произвольный ключ
# LoginManager = LoginManager(app)
menu = [            # Основное меню страницы
        {"name": "Регистрация", "url": "register"},
        {"name": "Вход", "url": "test"},
        {"name": "Выход", "url": "logout"},
        {"name": "Загрузка файлов", "url": "upload"},
        {"name": "Просмотр файлов", "url": "files"},
        {"name": "О сайте", "url": "about"}
                                                    ]

'''
Для подключения к базе данных в случае необходимости идентицикации через сервер баз данных
class UserLogin:
    def is_authentificated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.__user['id'])
'''


@app.route("/")
def index():
    return render_template('index.html', menu=menu)


@app.route("/about")
def about():

    return render_template('about.html', title="О сайте", menu=menu)


@app.route("/profile/<username>")       # Проверка сессии пользователя
def profile(username):
    if 'userLogged' not in session or session['userLogged'] != username:
        abort(401)

    return redirect(url_for('upload_file', username=session['userLogged']))


@app.route('/logout')
def logout():
    global user, password
    session.pop('username', None)
    user = ''
    password = ''
    return redirect(url_for('index'))


@app.route("/register", methods=["POST", "GET"])            # регистрация пользователя
def register():
    global user, password
    if request.method == "POST":
        user = request.form['username']
        password = request.form['psw']
        flash('Успешная регистрация', category='success')

    return render_template('register.html', title="Регистрация", menu=menu)


@app.route("/test", methods=["POST", "GET"])        # Вход пользователя после регистрации
def login():
    global user, password
    if 'userLogged' in session:
        return redirect(url_for('profile', username=session['userLogged']))
    if request.method == "POST":
        if request.form['username'] == user and request.form['psw'] == password:
            session['userLogged'] = request.form['username']
            return render_template('upload.html', title='Загрузка файла', menu=menu)
    return render_template('test.html', title="Авторизация", menu=menu)


@app.errorhandler(404)              # Страница ошибки
def pageNotFount(error):
    return render_template('page404.html', title="Страница не найдена", menu=menu), 404


with app.test_request_context():
    print(url_for('index'))


folder = './files'                          # загрузка файлов
app.config['UPLOAD_FOLDER'] = folder


@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if 'userLogged' not in session or session['userLogged'] != user:
        abort(401)
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('Пусто', category='error')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('Не выбран файл', category='error')
            return redirect(request.url)
        if file:
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('upload_file', filename=filename))

    return render_template('upload.html', title="Загрузка файла", menu=menu)

if os.environ['pathweb'] == "disable":
    files_index = AutoIndex(app, os.environ['pluspath'], add_url_rules=False)      # просмотр файлов
else:
    files_index = AutoIndex(app, eval(os.environ['pathweb'])+os.environ['pluspath'], add_url_rules=False)


@app.route('/files')
@app.route('/files/<path:path>')
def autoindex(path='.'):
    if 'userLogged' not in session or session['userLogged'] != user:
        abort(401)

    return files_index.render_autoindex(path)



if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', ssl_context=('cert.pem', 'key.pem'))
