FROM python:3


RUN pip install --upgrade pip

WORKDIR /app
COPY requirements.txt requirements.txt


RUN pip install --no-cache-dir --user -r requirements.txt

COPY . /app

ENTRYPOINT [ "python" ]

CMD [ "webtool.py" ]
